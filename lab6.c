#include <stdio.h>
int main() {
    int a[10][10], transpose[10][10], m, n, i, j;
    printf("Enter rows and columns: ");
    scanf("%d %d", &m, &n);
    printf("\nEnter matrix elements:\n");
    for (i = 0; i < m; i++)
     {
          for (j = 0; j < n; j++)
           {
             scanf("%d", &a[i][j]);
          }
     }
printf("\nEntered matrix: \n");
    for (i = 0; i < m; i++)
      {
          for (j = 0; j < n; j++) 
           {
            printf("%d  ", a[i][j]);
           }
           printf("\n");
     }
    for (i = 0; i < m; i++)
      {
          for (j = 0; j < n; j++)
             {
                 transpose[j][i] = a[i][j];
              }
       }
printf("\nTranspose of the matrix:\n");
    for (i = 0; i < m; i++)
      {
          for (j = 0; j < n; j++)
           {
               printf("%d\t ", transpose[i][j]);
            }
          printf("\n");
     }
    return 0;
}
