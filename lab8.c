#include<stdio.h>
#include<string.h>
int main()
{
  struct student
  {
       int roll_no;
       char name[80];
       char section[10];
       char department[50];
       float fees;
       float total_marks;
  };
struct student stud[2];
int i;
for(i=0;i<2;i++)
  {
         printf("\nEnter information of student %d:",i+1);
         printf("\nEnter name of student: ");
         scanf("%s",&stud[i].name);
         printf("Enter roll number of student: ");
        scanf("%d",&stud[i].roll_no);
        printf("Enter section of student: ");
        scanf("%s",&stud[i].section);
        printf("Enter department of student: ");
        scanf("%s",&stud[i].department);
        printf("Enter fees : ");
        scanf("%f",&stud[i].fees);
        printf("Enter total marks(result) obtained: ");
        scanf("%f",&stud[i].total_marks);
  }
if(stud[1].total_marks > stud[2].total_marks)
  i=1;
else
  i=2;
printf("\nDisplaying Information of student who scored highest :");
printf("\nName       : %s",stud[i].name);
printf("\nRoll number: %d",stud[i].roll_no);
printf("\nSection    : %s",stud[i].section);
printf("\nDepartment : %s",stud[i].department);
printf("\nFees in Rs : %f",stud[i].fees);
printf("\nResult     : %f\n",stud[i].total_marks);
return 0;
}
