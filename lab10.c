#include<stdio.h>
#include <math.h>
int main()
{
 float a, b, c;
 float root1, root2, imaginary;
 float d;
printf("Enter values of a, b, c of quadratic equation (aX^2 + bX + c):
\n");
scanf("%f%f%f", &a, &b, &c);
 d = (b*b)-(4*a*c);
 if(d > 0)
 {
 root1 = (-b + sqrt(d)) / (2*a);
 root2 = (-b - sqrt(d)) / (2*a);
 printf("The two real roots are : %.2f and %.2f", root1, root2);
 }
 else if(d== 0)
 {
 root1=root2= (-b / (2 * a));
 printf("The two real and equal roots are: %.2f and %.2f", root1,
root2);
 }
 else if(d < 0)
 {
 root1 = root2 = -b / (2 * a);
 imaginary = sqrt(-d) / (2 * a);
 printf("Two distinct complex roots exists: %.2f + i%.2f and %.2f -
i%.2f",
 root1, imaginary, root2, imaginary);
 }
 return 0;
}
