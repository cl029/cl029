#include <stdio.h>
int main()
{
 int a,b,*p,*q,sum,diff,prod,quo;
 float rem;
 printf("Enter two integers to add\n");
 scanf("%d%d", &a, &b);
 p = &a;
 q = &b;
 sum = *p + *q;
 diff= *p - *q;
 prod= *p * *q;
 quo = *p / *q;
 rem = *p % *q;
 printf("Sum of the numbers = %d\n", sum);
 printf("Difference of the numbers = %d\n", diff);
 printf("Product of the numbers = %d\n", prod);
 printf("Quotient of the numbers = %d\n", quo);
 printf("Remainder of the numbers = %f\n", rem);
 return 0;
}

